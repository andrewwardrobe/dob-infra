variable "subnets" {
  description = "subnet ids for lb placement"
  type = "list"
}

variable "name" {
  description = "load balancer/dns name"
}

variable "vpc_id" {
  description = "VPC id"
}

variable "hosted_zone_id" {
  description = "Hosted zone ID"
}