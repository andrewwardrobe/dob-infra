resource "aws_security_group" "alb" {
  vpc_id = "${var.vpc_id}"
  name = "${var.name} load balancer security group"
}

resource "aws_security_group_rule" "http_in" {
  from_port = 80
  protocol = "tcp"
  security_group_id = "${aws_security_group.alb.id}"
  cidr_blocks = ["0.0.0.0/0"]
  to_port = 80
  type = "ingress"
}

resource "aws_security_group_rule" "https_in" {
  from_port = 443
  protocol = "tcp"
  security_group_id = "${aws_security_group.alb.id}"
  cidr_blocks = ["0.0.0.0/0"]
  to_port = 443
  type = "ingress"
}

resource "aws_security_group_rule" "http_out" {
  from_port = 80
  protocol = "tcp"
  security_group_id = "${aws_security_group.alb.id}"
  cidr_blocks = ["0.0.0.0/0"]
  to_port = 80
  type = "egress"
}

resource "aws_security_group_rule" "https_out" {
  from_port = 443
  protocol = "tcp"
  security_group_id = "${aws_security_group.alb.id}"
  cidr_blocks = ["0.0.0.0/0"]
  to_port = 443
  type = "egress"
}

resource "aws_alb" "alb" {
  name = "${var.name}-lb"
  internal = false
  load_balancer_type = "application"
  security_groups = []
  subnets = ["${var.subnets}"]
}

resource "aws_route53_record" "alb" {
  name = "${var.name}"
  type    = "CNAME"
  ttl     = "300"
  zone_id = "${var.hosted_zone_id}"
  records = ["${aws_alb.alb.dns_name}"]
}

