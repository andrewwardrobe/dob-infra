resource "aws_vpc" "main" {
  cidr_block = "192.168.0.0/21"
  tags {
    Name = "${var.name}"
  }

}

resource "aws_default_network_acl" "default" {
  default_network_acl_id = "${aws_vpc.main.default_network_acl_id}"
  tags {
    Name = "Default NACL for VPC ${var.name}"
  }
}

resource "aws_default_route_table" "default" {
  default_route_table_id = "${aws_vpc.main.default_route_table_id}"
  tags {
    Name = "Default Route Table for VPC ${var.name}"
  }
}



