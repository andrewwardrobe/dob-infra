terraform {
  backend "s3" {
  }
}

data "terraform_remote_state" "state" {
  backend = "s3"
  config {
    bucket     = "${var.state_bucket}"
    region     = "${var.region}"
    key        = "${var.state_file}"
    profile    = "dob"
  }
}

provider "aws" {
  region = "${var.region}"
  profile = "dob"
}

module "vpc" {
  source = "./modules/vpc"
  name = "${var.vpc_name}"
}

data "aws_route53_zone" "public" {
  name = "daoostinboyeez.com"
}

resource "aws_route53_zone" "private" {
  name = "daoostinboyeez.com"
  vpc {
    vpc_id = "${module.vpc.id}"
  }
}

module "private_subnets" {
  source = "./modules/multi_az_private_subnets"
  cidr_block = "${cidrsubnet(module.vpc.cidr_block ,3 ,1 )}"
  vpc_id = "${module.vpc.id}"
  name = "Private Subnet for VPC ${var.vpc_name}"
}

module "public_subnets" {
  source = "./modules/public_subnets"
  cidr_block = "${cidrsubnet(module.vpc.cidr_block ,3 ,0 )}"
  vpc_id = "${module.vpc.id}"
  name = "${var.vpc_name}"
}


resource "aws_eip" "bastion" {
  vpc = true
  instance = "${module.bastion.instance_ids[0]}"
}

module "nat_gateway" {
  source = "./modules/nat_gateway"
  route_table_id = "${module.private_subnets.route_table_id}"
  subnet_id = "${module.public_subnets.ids[0]}"
}

module "bastion_security_group" {
  source = "./modules/bastion_security_group"
  vpc_id = "${module.vpc.id}"
}

module "private_security_group" {
  source = "./modules/private_security_group"
  vpc_id = "${module.vpc.id}"
  bastion_security_group_id = "${module.bastion_security_group.id}"
}

module "bastion" {
  source = "./modules/ec2_with_dns"
  name = "bastion"
  ami = "ami-f976839e"
  instance_type = "t2.micro"
  user_data = ""
  subnet_ids = ["${module.public_subnets.ids}"]
  route53_zone = "${data.aws_route53_zone.public.zone_id}"
  num_instances = "1"
  security_groups = ["${module.bastion_security_group.id}"]
  dns_entry = false
  public_dns = true
  key_name = "${var.key}"
}


module "inner" {
  source = "./modules/ec2_with_dns"
  name = "inner"
  ami = "ami-f976839e"
  instance_type = "t2.micro"
  user_data = ""
  subnet_ids = ["${module.private_subnets.ids}"]
  route53_zone = "${aws_route53_zone.private.zone_id}"
  num_instances = "1"
  security_groups = ["${module.private_security_group.id}"]
  key_name = "${var.key}"
  public_dns = false
}


module "www_asg" {
  source = "./modules/autoscaling_group"
  name = "www"
  ami = "ami-f976839e"
  instance_type = "t2.micro"
  user_data = ""
  subnet_ids = ["${module.private_subnets.ids}"]
  route53_zone = "${aws_route53_zone.private.zone_id}"
  num_instances = "1"
  security_groups = ["${module.private_security_group.id}"]
  key_name = "${var.key}"
  public_dns = false
}


module "www_alb" {
  source = "./modules/application_load_balancer"
  name = "www"
  hosted_zone_id = "${data.aws_route53_zone.public.zone_id}"
  vpc_id = "${module.vpc.id}"
  subnets = ["${module.public_subnets.ids}"]
}

resource "aws_alb_target_group" "www_tg" {
  name = "www-tg"
  port = 80
  protocol = "HTTP"
  vpc_id = "${module.vpc.id}"
}

resource "aws_autoscaling_attachment" "www" {
  autoscaling_group_name = "${module.www_asg.group_name}"
  alb_target_group_arn = "${aws_alb_target_group.www_tg.arn}"
}


resource "aws_security_group_rule" "www_http" {
  from_port = 80
  protocol = "tcp"
  source_security_group_id = "${module.www_alb.security_group_id}"
  security_group_id = "${module.private_security_group.id}"
  to_port = 80
  type = "ingress"
}

resource "aws_security_group_rule" "www_https" {
  from_port = 443
  protocol = "tcp"
  source_security_group_id = "${module.www_alb.security_group_id}"
  security_group_id = "${module.private_security_group.id}"
  to_port = 443
  type = "ingress"
}

resource "aws_route53_record" "bastion" {
  name = "bastion"
  type    = "A"
  ttl     = "300"
  zone_id = "${data.aws_route53_zone.public.zone_id}"
  records = ["${aws_eip.bastion.public_ip}"]
}


output "Bastion IP" {
  value = "${aws_eip.bastion.public_ip}"
}

output "Private CIDRS" {
  value = "${cidrsubnet(module.vpc.cidr_block ,1 ,1 )}"
}